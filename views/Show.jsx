import React from "react";


const myStyle = {
  margin: "2rem",
  padding: "2rem",
  color: "#ffffff",
  backgroundColor: "#000000",
};


const capitalize = function(word)
{
  const [firstCharacter, ...restWord] = word;
  return [firstCharacter.toUpperCase(), ...restWord].join("");
};


const Show = function({_id, pokemon})
{
  return (
    <div style={myStyle}>
      <h1>Gotta Catch 'Em All!</h1>
      <img src={pokemon[_id].img + ".jpg"} />
      <h2>{capitalize(pokemon[_id].name)}</h2>
      <a href={"/pokemon"}>Back</a>
    </div>
  );
}


export default Show;

