import React from "react";


const myStyle = {
  margin: "2rem",
  padding: "2rem",
  color: "#ffffff",
  backgroundColor: "#000000",
};


const capitalize = function(word)
{
  const [firstCharacter, ...restWord] = word;
  return [firstCharacter.toUpperCase(), ...restWord].join("");
};

const capitalCase = function(text, separator = " ")
{
  const textParts = text.split(separator);
  return textParts.map((textPart) =>
    {
      return capitalize(textPart);
    }
  ).join(separator);
};


const Index = function({ pokemon })
{
  return (
    <div style={myStyle}>
      <h1>See All The Pokemon!</h1>
      <ul >
      {pokemon.map((eachPokemon, index) =>
        {
          return (
            <li key={eachPokemon.img}>
              <a href={`/pokemon/${index}`}>
                {capitalize(eachPokemon.name)}
              </a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}


export default Index;

