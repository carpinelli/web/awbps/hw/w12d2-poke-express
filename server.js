const express = require("express");
const jsxViewEngine = require("jsx-view-engine");
const pokemon = require("./models/pokemon");


const app = express();
const PORT = 8083;


app.set("view engine", "jsx");
app.engine("jsx", jsxViewEngine());

app.get("/", (request, response, next) =>
  {
    const HTML = `
      <div>
        <h1>Welcome to the Pokemon App!</h1>
        <a href="/pokemon">Pokemon Index</a>
      </div>
    `;
    response.send(HTML);
  }
);
app.get("/pokemon", (request, response, next) =>
  {
    response.render("Index", { pokemon });
  }
);
app.get("/pokemon/:id", (request, response, next) =>
  {
    response.render("Show", { _id: request.params.id, pokemon });
  }
);


app.listen(PORT, () =>
  {
    console.log(`Listening on http://localhost:${PORT}`);
  }
);

